
###   ABOUT   ################################################################################

Podtrac, Version 1.0

Author:

 Matt Farina, aka, mfer
   matt.farina@gmail.com
   http://www.mattfarina.com

Requirements: 
  Audio 1.x

###   FEATURES   #############################################################################

- Converts the download audio url from the audio module into a Podtrac url. See http://www.podtrac.com for details
  and to sign up for the service.

###   INSTALLATION   #########################################################################

1. Download and unzip the Podtrac module into your modules directory.

2. Goto Administer > Site Building > Modules and enable Podtrac.

###   CHANGELOG   ############################################################################

1.0, 2008-07-03
----------------------
- Initial release
